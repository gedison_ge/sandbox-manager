package org.hspconsortium.sandboxmanager.repositories;

import org.hspconsortium.sandboxmanager.model.ContextParams;
import org.springframework.data.repository.CrudRepository;

public interface ContextParamsRepository extends CrudRepository<ContextParams, Integer> {
}
